%global girname LangTag
%global girapiversion 0.6
%global soversion 1
%global soversion_gobject 0

Name: liblangtag
Version: 0.6.4
Release: 2
Summary: An interface library to access tags for identifying languages

License: LGPLv3+ or MPLv2.0
URL: http://tagoh.bitbucket.org/liblangtag/
Source0: https://bitbucket.org/tagoh/%{name}/downloads/%{name}-%{version}.tar.bz2
Patch0: liblangtag-noparallel-gir.patch
Patch1: 0001-ro-MD-ro-to-get-make-check-to-succeed.patch
Patch2: fix_lose_stdlib.patch

Requires: %{name}-data = %{version}-%{release}

BuildRequires: glibc-common pkgconfig(check) pkgconfig(gobject-2.0) make gcc
BuildRequires: pkgconfig(gobject-introspection-1.0) libxml2-devel 
%if ! 0%{?flatpak}
BuildRequires: gtk-doc
%endif

%description
%{name} is an interface library to access tags for identifying
languages.

Features:
* several subtag registry database supports:
  - language
  - extlang
  - script
  - region
  - variant
  - extension
  - grandfathered
  - redundant
* handling of the language tags
  - parser
  - matching
  - canonicalizing

%package data
Summary: %{name} data files
License: Unicode
BuildArch: noarch

%description data
The %{name}-data package contains data files for %{name}.

%package gobject
Summary: GObject introspection for %{name}
Requires: %{name}%{?_isa} = %{version}-%{release}

%description gobject
The %{name}-gobject package contains files for GObject introspection for
%{name}.

%package devel
Summary: Development files for %{name}
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: %{name}-gobject%{?_isa} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package_help

%prep
%autosetup -p1

%build
%configure \
%if 0%{?flatpak}
    --disable-gtk-doc \
%endif
    --disable-silent-rules --disable-static --enable-shared --enable-introspection
sed -i \
    -e 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' \
    -e 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' \
    libtool
export LD_LIBRARY_PATH=`pwd`/liblangtag/.libs:`pwd`/liblangtag-gobject/.libs${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
%make_build

%install
%make_install
rm -f %{buildroot}/%{_libdir}/*.la %{buildroot}/%{_libdir}/%{name}/*.la

%ldconfig_scriptlets

%ldconfig_scriptlets gobject

%check
export LD_LIBRARY_PATH=`pwd`/liblangtag/.libs:`pwd`/liblangtag-gobject/.libs${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
%make_build check

%files
%doc AUTHORS NEWS README
%{_libdir}/%{name}.so.%{soversion}
%{_libdir}/%{name}.so.%{soversion}.*
%dir %{_libdir}/%{name}
%{_libdir}/%{name}/*.so

%files data
%license COPYING
%{_datadir}/%{name}

%files gobject
%{_libdir}/%{name}-gobject.so.%{soversion_gobject}
%{_libdir}/%{name}-gobject.so.%{soversion_gobject}.*
%{_libdir}/girepository-1.0/%{girname}-%{girapiversion}.typelib

%files devel
%{_includedir}/%{name}
%{_libdir}/%{name}.so
%{_libdir}/%{name}-gobject.so
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/pkgconfig/%{name}-gobject.pc
%{_datadir}/gir-1.0/%{girname}-%{girapiversion}.gir

%files help
%license COPYING
%{_datadir}/gtk-doc/html/%{name}

%changelog
* Thu Oct 31 2024 Ming Keke <keke.oerv@isrc.iscas.ac.cn> - 0.6.4-2
- Added patch fix_lose_stdlib.patch

* Wed Sep 06 2023 Darssin <2020303249@mail.nwpu.edu.cn> - 0.6.4-1
- Package init
